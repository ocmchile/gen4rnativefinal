import React from 'react'
// import SafeAreaView from 'react-native-safe-area-view';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from './screens/Login'
import Home from './screens/Home'
import Alertas from './screens/Alertas'
import DetalleAlerta from './screens/DetalleAlerta'
import CliIncognito from './screens/CliIncognito'
import HomeScreen from './screens/HomeScreen'
import Apollo from './screens/Apollo'
import CustomNavigationBar from './components/CustomNavigationBar'
import { Provider } from 'react-native-paper';

const Stack = createNativeStackNavigator();

const App = () => {
    return (
      <Provider>
        <NavigationContainer>
          <Stack.Navigator
            initialRouteName="Alertas"
            screenOptions={{
              header: (props) => <CustomNavigationBar {...props} />,
              headerShown: true,
            }}>
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Alertas" component={Alertas} />
            <Stack.Screen name="DetalleAlerta" component={DetalleAlerta} />
            <Stack.Screen name="CliIncognito" component={CliIncognito} />
            <Stack.Screen name="Apollo" component={Apollo} />
            <Stack.Screen name="HomeScreen" component={HomeScreen} options={{
          title: 'Home Screen',
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }} />
          </Stack.Navigator>
        </NavigationContainer>
        </Provider>
    )
}

export default App
