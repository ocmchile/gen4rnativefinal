import React from 'react'
import { View, Text, Image } from 'react-native'

const theme = {
  Button: {
    raised: true,
  },
};

const Header = () => {
    return (
        <View
            style=
                {{
                    padding: 5,
                    backgroundColor:'black'
                }}
        >
            <View
                style=
                    {{
                        flexDirection: 'row',
                        alignItems:'center',
                        borderColor:'red',
                        borderWidth:0
                    }}
            >
                <Text
                    style=
                        {{
                            flex: 1,
                            borderColor:'red',
                            borderWidth:0
                        }}
                >
                    <Image source={{uri:'http://www.tack.cl/newsiel/clientesboot2/img/logologin.png'}} style={{width: 120, height: 30}} />
                </Text>
                <Text
                    style=
                        {{
                            flex: 1,
                            borderColor:'red',
                            borderWidth:0,
                            textAlign: 'center',
                            fontSize: 18,
                            fontWeight: '700',
                            color: 'white'
                        }}
                >
                    NewSiel - Clientes
                </Text>
                <Text
                    style=
                        {{
                            flex: 1,
                            borderColor:'red',
                            borderWidth:0,
                            textAlign: 'right',
                            color: 'white'
                        }}
                >
                    Right
                </Text>
            </View>
        </View>
    )
}

export default Header
