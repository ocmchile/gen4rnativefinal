import React, { useState } from 'react';
import { View, Text } from 'react-native'
import { Menu, MenuItem, MenuDivider } from 'react-native-material-menu';

const MenuPrincipal = () => {
    const [visible, setVisible] = useState(true);
    const hideMenu = () => setVisible(false);
    const showMenu = () => setVisible(true);
    return (
        <>
            <View style={{ height: '100%', alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                <Menu
                    visible={visible}
                    anchor={<Text onPress={showMenu}>Show menu</Text>}
                    onRequestClose={hideMenu}
                >
                    <MenuItem onPress={hideMenu}>Menu item 1</MenuItem>
                    <MenuItem onPress={hideMenu}>Menu item 2</MenuItem>
                    <MenuItem disabled>Disabled item</MenuItem>
                    <MenuDivider />
                    <MenuItem onPress={hideMenu}>Menu item 4</MenuItem>
                </Menu>
            </View>
            <View style={{
                flex: 1,
                width: 500,
                height: 500,
                alignItems: 'flex-start',
                padding: 20,
            }}>
                <View style={{
                    flex: 1,
                    width: 100,
                    height: 100,
                }} />1
                <View style={{
                    flex: 1,
                    width: 100,
                    height: 100,
                    marginHorizontal: 20,
                    flexGrow: 1,
                }} />2
                <View style={{
                    flex: 1,
                    width: 100,
                    height: 100,
                }} />3
            </View>
        </>
    )
}

export default MenuPrincipal
