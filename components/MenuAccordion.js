import React from 'react'
import { View, Text, Button } from 'react-native'
import {Collapse,CollapseHeader, CollapseBody, AccordionList} from 'accordion-collapse-react-native';
import { Thumbnail, List, Separator } from 'native-base';

const ListItem = ({children}) => {return <View>{children}</View>};
const MenuAccordion = () => {
    return (
        <View
            style={{
        // height: 100,
        // flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 15,
      }}
        >
            <Collapse>
                <CollapseHeader>
                    <Separator bordered>
                    <Text>FORWARD</Text>
                    </Separator>
                </CollapseHeader>
                <CollapseBody>
                    <ListItem >
                    <Text>Aaron Bennet</Text>
                    </ListItem>
                    <ListItem>
                    <Text>Claire Barclay</Text>
                    </ListItem>
                    <ListItem last>
                    <Text>Kelso Brittany</Text>
                    </ListItem>
                </CollapseBody>
                </Collapse>
                <Collapse>
                <CollapseHeader>
                    <Text>FORWARD</Text>
                </CollapseHeader>
                <CollapseBody>
                    <ListItem >
                    <Text>Aaron Bennet</Text>
                    </ListItem>
                    <ListItem>
                    <Text>Claire Barclay</Text>
                    </ListItem>
                    <ListItem last>
                    <Text>Kelso Brittany</Text>
                    </ListItem>
                </CollapseBody>
                </Collapse>
        </View>
    )
}

export default MenuAccordion