import React, { useState } from 'react'
import { View, Text, Image } from 'react-native'
// import {useNavigation} from '@react-navigation/core';
import { Appbar, Button, Menu, Divider, Provider } from 'react-native-paper';

const CustomNavigationBar = ({ navigation, back }) => {
    const [visible, setVisible] = useState(false);
    const openMenu = () => setVisible(true);
    const closeMenu = () => setVisible(false);

    // const navigation = useNavigation();

    return (
        // <Provider>
        <Appbar.Header>
      {back ? <Appbar.BackAction onPress={navigation.goBack} /> : null}
      <Appbar.Content title="SIEL - Clientes" subtitle={'Desarrollado por OCM'} />
      <Image source={{uri:'http://www.tack.cl/newsiel/clientesboot2/img/logologin.png'}} style={{width: 120, height: 30}} />
      {!back ? (
        // <Provider>
        <View>
        <Menu
          visible={visible}
          onDismiss={closeMenu}
          anchor={
            <Appbar.Action icon="menu" color="white" onPress={openMenu} />
          }>
          <Menu.Item icon="home" onPress={() => navigation.navigate('HomeScreen')} title="Home" />
          <Menu.Item icon="account-alert" onPress={() => navigation.navigate('Alertas')} title="Alertas" />
          {/* <Menu.Item icon="calendar-arrow-right" onPress={() => {console.log('Option 3 was pressed')}} title="Programación" disabled /> */}
          <Menu.Item icon="account-search" onPress={() => navigation.navigate('CliIncognito')} title="Cliente Incógnito" />
          <Menu.Item icon="poll" onPress={() => {console.log('Option 3 was pressed')}} title="Encuestas" />
          {/* <Menu.Item icon="text-box-check-outline" onPress={() => {console.log('Option 3 was pressed')}} title="Auditorías" /> */}
          <Menu.Item icon="file-export" onPress={() => {console.log('Option 3 was pressed')}} title="Exportar" />
          <Menu.Item icon="file-chart-outline" onPress={() => {console.log('Option 3 was pressed')}} title="Informes" />
          <Divider />
          <Menu.Item icon="logout" onPress={() => navigation.navigate('Login')} title="Logout" />
        </Menu>
        </View>
    // </Provider>
      ) : null}
    </Appbar.Header>
    // </Provider>
    )
}

export default CustomNavigationBar
