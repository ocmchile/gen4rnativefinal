import React, {useState, useEffect} from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { DataTable } from 'react-native-paper';
import axios from 'axios';
import {useNavigation} from '@react-navigation/native';
import {Picker} from '@react-native-picker/picker';

// const numberOfItemsPerPageList = [2, 3, 4];

const Alertas = () => {
  const navigation = useNavigation();
    const [alertas, setAlertas] = useState([]);

    const [selectedAnio, setSelectedAnio] = useState();
    const [selectedMes, setSelectedMes] = useState();
    

    // const [page, setPage] = React.useState(0);

    // const [numberOfItemsPerPage, onItemsPerPageChange] = React.useState(numberOfItemsPerPageList[0]);

    // const from = page * numberOfItemsPerPage;

    useEffect(() => {
      axios
        .get('https://www.ocmltda.com/DESA/TACK/appescalab/api/sielapi.php/alertas/2020/09')
        .then(e => setAlertas(e.data));
        // setPage(0);
    }, []);
    // }, [numberOfItemsPerPage]);

    // const to = Math.min((page + 1) * numberOfItemsPerPage, alertas.length);

  //   React.useEffect(() => {
  //     setPage(0);
  //  }, [numberOfItemsPerPage]);

  //console.log('lala');
  console.log(alertas);

    return (
        <ScrollView>
      <Text>ALERTAS</Text>
      <View style={{flex: 1, flexDirection: 'row', alignItems: 'flex-start', borderColor: '#000000', borderWidth: 2, margin: 5, padding: 5}}>
        <Text style={{alignSelf: 'flex-start'}}>Año </Text>
        <Picker selectedValue={selectedAnio}
  onValueChange={(itemValue, itemIndex) =>
    setSelectedAnio(itemValue)
  } style={{ width: '28%', alignSelf: 'flex-start' }}>
            <Picker.Item label="2020" value="2020" />
            <Picker.Item label="2019" value="2019" />
        </Picker>
        <Text style={{alignSelf: 'flex-start'}}> Mes </Text>
        <Picker selectedValue={selectedMes}
  onValueChange={(itemValue, itemIndex) =>
    setSelectedMes(itemValue)
  } style={{ width: '40%', alignSelf: 'flex-start' }}>
            <Picker.Item label="Enero" value="01" />
            <Picker.Item label="Febrero" value="02" />
            <Picker.Item label="Marzo" value="03" />
            <Picker.Item label="Abril" value="04" />
            <Picker.Item label="Mayo" value="05" />
            <Picker.Item label="Junio" value="06" />
            <Picker.Item label="Julio" value="07" />
            <Picker.Item label="Agosto" value="08" />
            <Picker.Item label="Septiembre" value="09" />
            <Picker.Item label="Octubre" value="10" />
            <Picker.Item label="Noviembre" value="11" />
            <Picker.Item label="Diciembre" value="12" />
        </Picker>
        </View>
      <DataTable>
      {/* <DataTable.Pagination
        page={page}
        numberOfPages={Math.ceil(alertas.length / numberOfItemsPerPage)}
        onPageChange={page => setPage(page)}
        label={`${from + 1}-${to} of ${alertas.length}`}
        showFastPaginationControls
        numberOfItemsPerPageList={numberOfItemsPerPageList}
        numberOfItemsPerPage={numberOfItemsPerPage}
        onItemsPerPageChange={onItemsPerPageChange}
        selectPageDropdownLabel={'Rows per page'}
      /> */}
        <DataTable.Header>
          <DataTable.Title sortDirection='ascending'>ID</DataTable.Title>
          <DataTable.Title>Cliente</DataTable.Title>
          <DataTable.Title>Fecha</DataTable.Title>
          <DataTable.Title>Hora</DataTable.Title>
        </DataTable.Header>
        {alertas.map((item, index) => {
            return (
                <DataTable.Row key={index} onPress={() => navigation.navigate('DetalleAlerta', {id: item.id})}>
                    <DataTable.Cell>{item.id}</DataTable.Cell>
                    <DataTable.Cell>{item.cli_nombre}</DataTable.Cell>
                    <DataTable.Cell>{item.fecha}</DataTable.Cell>
                    <DataTable.Cell>{item.hora}</DataTable.Cell>
                </DataTable.Row>
            );
        })}
      </DataTable>
      {/* {alertas.map(item => {
        return (
          <View>
            
            <Text>{item.id}</Text>
          </View>
        );
      })} */}
    </ScrollView>
    )
}

export default Alertas
