import React, {useState, useEffect} from 'react'
import { View, Text, Image, Button, StyleSheet } from 'react-native'
import axios from 'axios';

const DetalleAlerta = ({route}) => {
    const [detalerta, setDetAlerta] = useState([]);

    const {id} = route.params;

    useEffect(() => {
        axios
          .get('https://www.ocmltda.com/DESA/TACK/appescalab/api/sielapi.php/alertas/' + route.params.id)
          .then(e => setDetAlerta(e.data));
      }, []);

    
    return (
        <>
        <View>
        {detalerta.map((item, index) => {
            return (
                <View key={index} style={{margin: 5, padding: 5}}>
                <Image
                source={{uri: 'http://www.tack.cl/newsiel/uploads/clients-logo/' + item.cli_logo}}
                style={{
                    height: 150,
                    width: '99%',
                    marginVertical: 20,
                    borderRadius: 10,
                    resizeMode: 'stretch',
                    borderColor: '#000000',
                    borderWidth: 2
                }}
                />
                <Text style={{color: '#000000', fontWeight: '500'}}>ID Alerta: {id}</Text>
                <Text style={{color: '#000000', fontWeight: '500'}}>FECHA ALERTA: {item.fecha}</Text>
                <Text style={{color: '#000000', fontWeight: '500'}}>HORA ALERTA: {item.hora}</Text>
                <Text style={{color: '#000000', fontWeight: '500'}}>CLIENTE: {item.cli_nombre}</Text>
                <Text style={{color: 'red', fontWeight: '500'}}>MOTIVO ALERTA: {item.tipi_nombre}</Text>
                <Text style={{color: '#000000', fontWeight: '500'}}>COMENTARIO: {item.comentarios}</Text>
                </View>
            );
        })}
        </View>
        {/* <Button title="Open DatePickerAndroid"
            onPress={ async () => {
            const { action, year, month, day } = await DatePickerAndroid.open({
            date : new Date()
            });
            }}
            /> */}
        </>
    )

    const stylesAPITest = StyleSheet.create({
        style1 : { flex : 1, fontSize : 12, color : "red" },
        style2 : { color : "blue" },
        });
}

export default DetalleAlerta
