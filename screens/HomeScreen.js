import React from 'react';
import {View, Text, Button, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/core';

const HomeScreen = () => {
    const navigation = useNavigation();
    
    return (
        <View style={style.container}>
      <Text>Home Screen</Text>
      <Button
        title="Go to login"
        onPress={() => navigation.navigate('Login')}
      />
    </View>
    )
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default HomeScreen