import React from 'react'
import { View, Text, TextInput, TouchableOpacity, Button, useWindowDimensions } from 'react-native';
import {useNavigation} from '@react-navigation/core';
import Header from '../components/Header'

const Login = () => {
    const navigation = useNavigation();

    const height = useWindowDimensions().height;

    return (
        <>
        {/* <Header /> */}
        <View style={{ alignItems: 'center', backgroundColor: 'lightgrey', height: height, flex: 1 }}>
            <Text style={{
                fontSize: 24,
                padding: 10,
                fontWeight: '600'
            }}>LOGIN</Text>
            <Text>Username</Text>
            <TextInput
                placeholder="Nombre de usuario"
                style={{
                    backgroundColor: 'white',
                    width: '90%',
                    margin: 20,
                    borderRadius: 10,
                    padding: 10,
                    fontSize: 18,
                }}
            />
            <Text>Password</Text>
            <TextInput
                placeholder="Password"
                style={{
                    backgroundColor: 'white',
                    width: '90%',
                    margin: 20,
                    borderRadius: 10,
                    padding: 10,
                    fontSize: 18,
                }}
            />
                <Button
                    onPress={() => {
                navigation.navigate('Home');
            }}
                    title="Login"
                />
            <TouchableOpacity onPress={() => {
                navigation.navigate('Home');
            }}>
            </TouchableOpacity>
        </View>
        </>
    )
}

export default Login
