import React from 'react';
import { View, Text, Button } from 'react-native';
import {useNavigation} from '@react-navigation/core';
// import MenuAccordion from '../components/MenuAccordion'
import MenuPrincipal from '../components/MenuPrincipal';
import Header from '../components/Header'

const Home = () => {
    const navigation = useNavigation();

    return (
        <>
        {/* <Header /> */}
        <View
            style={{
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 15,
        flex: 1
      }}
        >
            <Text>ESTE ES EL HOME, Y ESTA EL MENU PRINCIPAL</Text>
            <Button
                    style={{width: '20%'}}
                    onPress={() => {
                        navigation.navigate('HomeScreen');
                    }}
                    title="Menu Principal"
                />
            <MenuPrincipal />
        </View>
        </>
    )
}

export default Home
