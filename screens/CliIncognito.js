import React, {useState, useEffect} from 'react';
import { View, Text, Image, ScrollView } from 'react-native';
import { DataTable } from 'react-native-paper';
import axios from 'axios';

const CliIncognito = () => {
    const [incognitos, setIncognitos] = useState([]);

  useEffect(() => {
    axios
      .get('https://www.ocmltda.com/DESA/TACK/appescalab/api/sielapi.php/visitasClienteIncognito/2021/05')
      .then(e => setIncognitos(e.data));
  }, []);

  //console.log('lala');
  // console.log(alertas);

    return (
        <ScrollView>
      <Text>VISITAS CLIENTE INCÓGNITO</Text>
      <DataTable>
        <DataTable.Header>
          <DataTable.Title sortDirection='ascending'>ID</DataTable.Title>
          <DataTable.Title>Fecha</DataTable.Title>
          <DataTable.Title>Local</DataTable.Title>
        </DataTable.Header>
        {incognitos.map((item, index) => {
            return (
                <DataTable.Row key={index}>
                    <DataTable.Cell>{item.vis_id}</DataTable.Cell>
                    <DataTable.Cell>{item.vis_fecha}</DataTable.Cell>
                    <DataTable.Cell>{item.loc_nombre}</DataTable.Cell>
                </DataTable.Row>
            );
        })}
      </DataTable>
      {/* {alertas.map(item => {
        return (
          <View>
            
            <Text>{item.id}</Text>
          </View>
        );
      })} */}
    </ScrollView>
    )
}

export default CliIncognito
